# IoT ontologies developed by the MELODI research team

The ontologies available in this repository are developed by the [MELODI team](https://www.irit.fr/-Equipe-MELODI-), a research group of [IRIT](https://www.irit.fr/). They are developed for research or teaching purpose.  

## IoT-O ![ReLOVStats restrictions](https://lov.ilabt.imec.be/lovcube/data/relovstats/20190405/voc/ioto?type=svg)

[IoT-O](https://www.irit.fr/recherches/MELODI/ontologies/IoT-O) is a modular core-domain IoT ontology. Its design process has been documented in a [scientific publication at the EKAW conference](https://hal.archives-ouvertes.fr/hal-01467853v1). The purpose of IoT-O is to provide a unifying frame to describe IoT knowledge. In order to provide as much interoperability as possible, IoT-O reuses as much as possible existing ontologies, such as [SSN](www.w3.org/ns/ssn/). IoT-O is also compliant with the [LOV](lov.linkeddata.es) requirements.

## SAN ![ReLOVStats restrictions](https://lov.ilabt.imec.be/lovcube/data/relovstats/20190405/voc/san?type=svg)

[SAN](https://www.irit.fr/recherches/MELODI/ontologies/SAN) is an ontology for connected actuators, developed to be a module for IoT-O. It mirrors SSN, which is dedicated to sensors.
